import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendeeForm from './AttendeeForm';
import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  const router = createBrowserRouter([
    {
      path: "/",
      element: (
        <>
          <Nav />
          <div className="my-5 container">
            <Outlet />
          </div>
        </>
      ),
      children: [
        { index: true, element: <MainPage /> },
        {
          path: "locations",
          children: [
            {path: "new", element: <LocationForm />}
          ]
        },
        {
          path: "conferences",
          children: [
            {path: "new", element: <ConferenceForm />}
          ]
        },
        {
          path: "attendees",
          children: [
            {path: "", element: <AttendeesList attendees={props.attendees} />},
            {path: "new", element: <AttendeeForm />}
          ]
        },
        {
          path: "presentations",
          children: [
            {path: "new", element: <PresentationForm />}
          ]
        }
      ]
    }
  ]);
  return <RouterProvider router={router}/>

  // return (
  //   <>
  //     <Nav />
  //     <div className="container">
  //       {/* <LocationForm /> */}
  //       {/* <ConferenceForm /> */}
  //       {/* <PresentationForm /> */}
  //       <AttendeeForm />
  //       {/* <AttendeesList attendees={props.attendees} /> */}
  //       </div>
  //   </>
  // );
}

export default App;
