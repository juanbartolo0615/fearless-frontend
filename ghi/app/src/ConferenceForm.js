import React, { useEffect, useState } from "react";

function ConferenceForm(props) {
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [presentations, setPresentation] = useState('');
    const [attendees, setAttendee] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setPresentation(value);
    }
    const handleAttendeeChange = (event) => {
        const value = event.target.value;
        setAttendee(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const fetchData = async () => {
        const locations_url = 'http://localhost:8000/api/locations/';

        const response = await fetch(locations_url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setLocations(data.locations);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const conference_url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conference_url, fetchConfig);
        if (response.ok) {
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setPresentation('');
            setAttendee('');
            setLocation('');
        }
    }

    useEffect(() => {
        fetchData();
      }, []);
    return (
        <>
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>

                        <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name"id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartsChange}  value={starts} placeholder="Start Date" required type="datetime-local" name="starts" id="starts" className="form-control"/>
                            <label htmlFor="room_count">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndsChange} value={ends} placeholder="End Date" required type="datetime-local" name="ends" id="ends" className="form-control"/>
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <textarea onChange={handleDescriptionChange} value={description} className="form-control" name="description" id="description" rows="5"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePresentationChange} value={presentations} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                            <label htmlFor="room_count">Maximum Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAttendeeChange} value={attendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                            <label htmlFor="room_count">Maximum Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                                <option defaultValue="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>{location.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>

                    </div>
                </div>
            </div>
    </div>
        </>
    );
}

export default ConferenceForm;
